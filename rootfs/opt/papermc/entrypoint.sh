#!/bin/bash

set -e
set -u
set -o pipefail

cd /opt/papermc

function main() {
    envsubst -i server.properties -o server.properties
    envsubst -i paper.yml -o paper.yml
    envsubst -i bukkit.yml -o bukkit.yml
    envsubst -i spigot.yml -o spigot.yml

    mkdir -p /opt/papermc/plugins

    if [ "${PLUGIN_WORLDEDIT:-}" == "true" ]; then
        echo "Enabling plugin: WorldEdit 7.2.7"
        cp /opt/papermc/plugins-available/worldedit-bukkit-7.2.7.jar /opt/papermc/plugins/worldedit-bukkit-7.2.7.jar
    fi

    java -jar paper.jar
}

main "${@}"
