FROM registry.gitlab.com/javacraft/containers/java:17.0.1-1

ARG PAPERMC_VERSION=1.17.1-399

RUN cd /tmp && \
    curl -L https://papermc.io/api/v2/projects/paper/versions/1.17.1/builds/399/downloads/paper-${PAPERMC_VERSION}.jar -o paper-${PAPERMC_VERSION}.jar && \
    mkdir -p /opt/papermc && \
    mv paper-${PAPERMC_VERSION}.jar /opt/papermc/ && \
    ln -s /opt/papermc/paper-${PAPERMC_VERSION}.jar /opt/papermc/paper.jar

WORKDIR /opt/papermc

RUN cd /opt/papermc && \
    java -jar paper.jar

ADD ./rootfs/ /

RUN chmod +x /opt/papermc/entrypoint.sh

RUN cd /opt/papermc && \
    rm -f world || true && \
    rm -f world_the_end || true && \
    rm -f world_nether || true && \
    mkdir worlds

EXPOSE 25565

ENV PLUGIN_WORLDEDIT "false"

VOLUME [ "/opt/papermc/worlds" ]

ENTRYPOINT [ "/bin/bash", "-c" ]

CMD [ "/opt/papermc/entrypoint.sh" ]
# CMD [ "java -jar paper.jar" ]
