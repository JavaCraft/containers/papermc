# PaperMC container image

## Storage directory

The storage directory is set to `SERVER_WORLDS_STORAGE`.

## Supported environment variables

| Variable                        | Default value | Allowed values                       | Description |
|---------------------------------|---------------|--------------------------------------|-------------|
| `GAME_MODE`                     | `survival`    | `survival`, `creative`, `adventure`  | Sets the game mode for new players.
| `GAME_DIFFICULTY`               | `peaceful`    | `peaceful`, `easy`, `normal`, `hard` | Sets the difficulty of the world.
| `GAME_MAX_PLAYERS`              | `20`          | positive integer                     | The maximum number of players.
| `GAME_ONLINE`                   | `false`       | `false`, `true`                      | If true then only players with the original client can join game.
| `GAME_VIEW_DISTANCE`            | `35`          | range from `5` to `99`               | The maximum player view distance.
| `SERVER_PORT`                   | `25565`       | range from `1` to `65535             | IPv4 port on which server listen for players.
| `SERVER_WORLDS_STORAGE`         | `worlds`      | string                               | The worlds storage directory.
| `ENABLE_WHITELIST`              | `false`       | `false`, `true`                      | If true then only players on whitelist can join game.
| `ENABLE_WORLD_MAIN`             | `true`        | `false`, `true`                      | If true, the "world" will be generated.
| `ENABLE_WORLD_NETHER`           | `true`        | `false`, `true`                      | If true, the "nether" will be generated.
| `ENABLE_WORLD_THE_END`          | `true`        | `false`, `true`                      | If true, the "end" world will be generated.
| `GAME_LEVEL_NAME`               | `world`       | any string value                     | Name of the world.
| `GAME_LEVEL_TYPE`               | `default`     | `default`, `flat`                    | 
| `GAME_LEVEL_SEED`               | ` `           | any value                            | The seed used to generate level. |
| `GAME_LEVEL_GENERATOR`          |               |                                      |                
| `GAME_LEVEL_GENERATOR_SETTINGS` |               |                                      |

## Plugins

To enable plugin, set the environment variable from following list to `true`.

| Plugin     | Version | Variable           | Default |
|------------|---------|--------------------|---------|
| WorldEedit | `7.2.7` | `PLUGIN_WORLDEDIT` | `false` |
